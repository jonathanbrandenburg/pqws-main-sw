/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2017 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PQWS_H_
#define PQWS_H_

typedef struct {
  uint32_t sysTick;
  float battVolt;
  float inputVolt;
} pqws_system_status_t;

typedef struct {
  int8_t temperature;
  uint8_t humidity;
  uint16_t pressure;
  uint32_t gas1_ppm;
  uint32_t gas2_ppm;
  uint32_t gas3_ppm;
  uint32_t gas4_ppm;
} pqws_sensor_data_t;
#endif /* PQWS_H_ */
