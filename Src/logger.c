/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2017 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "fatfs.h"
#include "pqws.h"
uint8_t
logSystemStatus (pqws_system_status_t data)
{
  int res;
  FIL FLog;

  res = f_open (&FLog, "SYS/SYSTEM.LOG", FA_OPEN_APPEND | FA_WRITE);
  if (res != FR_OK)
    return res;

  if (res != FR_OK)
    return res;

  res = f_printf (&FLog, "PQWS - Tick: %d, Battery: %f V, Solar: %f V\n",
                  data.sysTick, data.battVolt, data.inputVolt);
  if (res == -1)
    return res;

  f_close (&FLog);
  return FR_OK;
}

uint8_t
logSensorData (pqws_sensor_data_t data)
{
  int res;
  FIL FLog;
  res = f_open (&FLog, "DATA/SENSOR.LOG", FA_OPEN_APPEND | FA_WRITE);
  if (res != FR_OK)
    return res;

  if (res != FR_OK)
    return res;

  res = f_printf (
      &FLog,
      "T:%d C H:%d %% P:%d bar, G1:%d ppm, G2:%d ppm, G3:%d ppm, G4:%d ppm\n",
      data.temperature, data.humidity, data.pressure, data.gas1_ppm,
      data.gas2_ppm, data.gas3_ppm, data.gas4_ppm);
  if (res == -1)
    return res;

  f_close (&FLog);
  return FR_OK;
}
